/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2_2_tambahan;

/**
 *
 * @author Lenovo
 */
public class Manager extends Pegawai implements TugasBelajar {

    private int jumAnak;
    private int jamKerja;
    private int istri;

    public Manager() {
        
    }

    public void setJumAnak(int jumAnak) {
        this.jumAnak = jumAnak;
    }

    public void setJamKerja(int jamKerja) {
        this.jamKerja = jamKerja;
    }

    public void setIstri(int istri) {
        this.istri = istri;
    }

    public int getJumAnak() {
        return jumAnak;
    }

    public int getJamKerja() {
        return jamKerja;
    }

    public int getIstri() {
        
        return istri;
    }

    public double hitTunjangan() {
        double tunjangan = 0;

        if (jumAnak > 0 && jumAnak <= 3) {
            tunjangan = jumAnak * 100000;
            return tunjangan;
        }

        return tunjangan;

    }

    public double hitLembur() {
        double gajiLembur = 0;

        if (jamKerja > 8) {
            gajiLembur = (jamKerja - 8) * 50000;
            return gajiLembur;
        }

        return gajiLembur;
    }

    @Override
    public double hitungGatot() {
        double gatot;
        gatot = hitGajiPokok() + hitTunjangan() + hitLembur();
        return gatot;
    }
    
    @Override
    public boolean isSelesai(){
        if(jamKerja > 8){
            return true;
        }else{
            return false;
        }
    }
    
}
