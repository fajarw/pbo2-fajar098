/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2_2_tambahan;

/**
 *
 * @author Lenovo
 */
public class Kantor {

    private Manager manager;
    private Pegawai[] pegawai;

    public Kantor() {

    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public void setPegawai(Pegawai[] pegawai) {
        this.pegawai = pegawai;
    }

    public Manager getManager() {
        return manager;
    }

    public Pegawai[] getPegawai() {
        return pegawai;
    }

    public double hitGajiPeg() {
        double gajiPeg = 0;

        for (int q = 0; q < pegawai.length; q++) {
            gajiPeg += pegawai[q].hitungGatot();
        }
        return gajiPeg;
    }
}
