/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2_2_tambahan;

/**
 *
 * @author Lenovo
 */
public class Pegawai implements Pendapatan{
    
    protected String nip;
    protected String nama;
    protected int golongan;
    protected double gajiTotal;
    
    public Pegawai(){
        
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setGolongan(int golongan) {
        this.golongan = golongan;
    }

    public String getNip() {
        return nip;
    }

    public String getNama() {
        return nama;
    }

    public int getGolongan() {
        return golongan;
    }

    public double hitGajiPokok(){
        double gajiPokok = 0;
        
        if(golongan == 1){
            gajiPokok = 500000;
            return gajiPokok;
        }else if(golongan == 2){
            gajiPokok = 750000;
            return gajiPokok;
        }else if(golongan ==3){
            gajiPokok = 1000000;
            return gajiPokok;
        }else{
            return gajiPokok;
        }
    }
    
    @Override
    public double hitungGatot() {
        return 0;
    }
    
    
}
