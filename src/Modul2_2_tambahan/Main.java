/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2_2_tambahan;

/**
 *
 * @author Lenovo
 */
public class Main {

    public static void main(String[] args) {

        Kantor kantor = new Kantor();

        Manager manager = new Manager();
        manager.setNip("p.9925");
        manager.setNama("Agung Bramantio");
        manager.setGolongan(3);
        manager.setJumAnak(4);
        manager.setJamKerja(7);
        manager.setIstri(1);

        kantor.setManager(manager);

        Marketing pgw1 = new Marketing();
        pgw1.setNip("p.1304");
        pgw1.setNama("Fajar Waskito");
        pgw1.setGolongan(2);
        pgw1.setJamKerja(5);

        Marketing pgw2 = new Marketing();
        pgw2.setNip("p.1305");
        pgw2.setNama("Ibnu Hariadi");
        pgw2.setGolongan(2);
        pgw2.setJamKerja(10);

        Honorer pgw3 = new Honorer();
        pgw3.setNip("p.1401");
        pgw3.setNama("Putri Juwita");
        pgw3.setGolongan(1);

        Pegawai[] pgw = new Pegawai[4];
        pgw[0] = manager;
        pgw[1] = pgw1;
        pgw[2] = pgw2;
        pgw[3] = pgw3;

        kantor.setPegawai(pgw);

        System.out.println("Nama Manager  : " + kantor.getManager().getNama());
        System.out.println("Daftar Pegawai");
        System.out.println("====================================================");
        System.out.printf("%-5s", "No.");
        System.out.printf("%-11s", "Nip");
        System.out.printf("%-11s", "Nama");
        System.out.printf("%-10s", "Studi");
        System.out.printf("%-12s", "Gaji");
        System.out.println("\n----------------------------------------------------");
        int no = 0;
        for (int q = 0; q < pgw.length; q++) {
            no++;

            if (kantor.getPegawai()[q] instanceof Marketing) {

                Marketing mark;
                mark = (Marketing) kantor.getPegawai()[q];
                System.out.printf("%-4s", no + ".");
                System.out.printf("%-8s", mark.getNip());
                System.out.printf("%-15s", mark.getNama());
                String studi;
                if (mark.isSelesai() == true) {
                    studi = "Selesai";
                } else {
                    studi = "studi";
                }
                System.out.printf("%-9s", studi);
                System.out.printf("%-12s", mark.hitungGatot());
                System.out.println();

            } else if (kantor.getPegawai()[q] instanceof Honorer) {
                Honorer hon;
                hon = (Honorer) kantor.getPegawai()[q];
                System.out.printf("%-4s", no + ".");
                System.out.printf("%-8s", hon.getNip());
                System.out.printf("%-15s", hon.getNama());
                System.out.printf("%-9s", "-");
                System.out.printf("%-12s", hon.hitungGatot());
                System.out.println();
            } else {
                no--;
            }

        }
        System.out.println("====================================================");
        System.out.println("Jumlah Gaji Kantor    : " + kantor.hitGajiPeg());
        System.out.println("====================================================");
    }
}
