/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2_2_tambahan;

/**
 *
 * @author Lenovo
 */
public class Marketing extends Pegawai implements TugasBelajar{
    
    private int jamKerja;
    
    public Marketing(){
        
    }

    public void setJamKerja(int jamKerja) {
        this.jamKerja = jamKerja;
    }

    public int getJamKerja() {
        return jamKerja;
    }
    
    public double hitLembur(){
        
        double gajiLembur = 0;
        
        if(jamKerja > 8){
            gajiLembur = (jamKerja - 8) * 50000;
            return gajiLembur;
        }
        
        return gajiLembur;
        
    }
    
    @Override
    public double hitungGatot(){
        double gatot;
        gatot = hitGajiPokok() + hitLembur();
        return gatot;
    }
    
    @Override
    public boolean isSelesai(){
        if(jamKerja > 8){
            return true;
        }else{
            return false;
        }
    }
}
