/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

import java.util.logging.Logger;

/**
 *
 * @author basisdata
 */
public class Masyarakat extends Penduduk implements Peserta{

    private String nomor;

    public Masyarakat() {

    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getNomor() {
        return nomor;
    }

    @Override
    public double hitungIuran() {
        double iuran = (Integer.parseInt(nomor) * 100);
        return iuran;
    }

    @Override
    public String getJenisSertifikat() {
        String sertif = "Peserta";
        return sertif;
    }

    @Override
    public String getFasilitas() {
        String fasilitas = "block note, alat tulis, dan modul pelatihan";
        return fasilitas;
    }

    @Override
    public String getKonsumsi() {
        String konsum = "snack, makan siang";
        return konsum;
    }

}
