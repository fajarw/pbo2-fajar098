/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;
/**
 *
 * @author basisdata
 */
public class Main {//adalah kelas dimana objek-objek di manipulasi

    public static void main(String[] args) {

        UKM ukm = new UKM();
        ukm.setNamaUnit("Natas");

        Mahasiswa mhs1 = new Mahasiswa();
        mhs1.setNama("Fajar Waskito");
        mhs1.setTanggalLahir("13 April 2000");
        mhs1.setNim("185314098");

        ukm.setKetua(mhs1);

        Mahasiswa mhs2 = new Mahasiswa();
        mhs2.setNama("Susanti");
        mhs2.setTanggalLahir("20 Sepetember 2001");
        mhs2.setNim("195314071");

        ukm.setSekretaris(mhs2);

        Mahasiswa mhs3 = new Mahasiswa();
        mhs3.setNama("Rifky Juliandra");
        mhs3.setTanggalLahir("9 Januari 2000");
        mhs3.setNim("185314003");

        Masyarakat msy1 = new Masyarakat();
        msy1.setNama("Abdul Zaelani");
        msy1.setTanggalLahir("11 Mei 1999");
        msy1.setNomor("191");

        Masyarakat msy2 = new Masyarakat();
        msy2.setNama("Toni Rena");
        msy2.setTanggalLahir("30 Februari 2001");
        msy2.setNomor("172");

        Penduduk[] anggotaUkm = new Penduduk[5];
        anggotaUkm[0] = mhs1;
        anggotaUkm[1] = mhs2;
        anggotaUkm[2] = mhs3;
        anggotaUkm[3] = msy1;
        anggotaUkm[4] = msy2;

        ukm.setAnggota(anggotaUkm);

        System.out.println("Nama UKM              : " + ukm.getNamaUnit());
        System.out.println("Nama Ketua            : " + ukm.getKetua().getNama());
        System.out.println("Nama Sekretaris       : " + ukm.getSekretaris().getNama());
        System.out.println("DAFTAR ANGGOTA");
        
        System.out.println("===================================================="
                + "=============================================================================");
        System.out.printf("%-4s", "No.");
        System.out.printf("%7s", "Nama");
        System.out.printf("%16s", "Iuran");
        System.out.printf("%16s", "Sertifikat");
        System.out.printf("%22s", "Fasilitas");
        System.out.printf("%48s", "Konsumsi");
        System.out.println("\n=================================================="
                + "===============================================================================");

        for (int q = 0; q < ukm.getAnggota().length; q++) {
            if (ukm.getAnggota()[q] instanceof Mahasiswa) {
                Mahasiswa mhs;
                mhs = (Mahasiswa) ukm.getAnggota()[q];

                if (ukm.getAnggota()[q] == ukm.getKetua()
                        || ukm.getAnggota()[q] == ukm.getSekretaris()) {
                    System.out.printf("%-4s", (q + 1) + ".");
                    System.out.printf("%-17s", mhs.getNama());
                    System.out.printf("%-13s", "   -");
                    System.out.printf("%-12s", mhs.getJenisSertifikat());
                    System.out.printf("%-47s", mhs.getFasilitas());
                    System.out.printf("%-25s", mhs.getKonsumsi());
                    System.out.println();

                } else {
                    System.out.printf("%-4s", (q + 1) + ".");
                    System.out.printf("%-17s", mhs.getNama());
                    System.out.printf("%-13s", mhs.hitungIuran());
                    System.out.printf("%-12s", mhs.getJenisSertifikat());
                    System.out.printf("%-47s", mhs.getFasilitas());
                    System.out.printf("%-25s", mhs.getKonsumsi());
                    System.out.println();
                }

            } else if (ukm.getAnggota()[q] instanceof Masyarakat) {
                Masyarakat msy;
                msy = (Masyarakat) ukm.getAnggota()[q];

                System.out.printf("%-4s", (q + 1) + ".");
                System.out.printf("%-17s", msy.getNama());
                System.out.printf("%-13s", msy.hitungIuran());
                System.out.printf("%-12s", msy.getJenisSertifikat());
                System.out.printf("%-47s", msy.getFasilitas());
                System.out.printf("%-25s", msy.getKonsumsi());
                System.out.println();
            }
        }
        System.out.println("----------------------------------------------------"
                + "-----------------------------------------------------------------------------");

        double total = 0;
        for (int q = 0; q < ukm.getAnggota().length; q++) {

            if (q > 1) {
                if (ukm.getAnggota()[q] instanceof Mahasiswa) {
                    Mahasiswa mhs;
                    mhs = (Mahasiswa) ukm.getAnggota()[q];
                    total += mhs.hitungIuran();

                } else if (ukm.getAnggota()[q] instanceof Masyarakat) {
                    Masyarakat msy;
                    msy = (Masyarakat) ukm.getAnggota()[q];
                    total += msy.hitungIuran();
                }

            } else {
                total = total + 0;
            }
        }

        System.out.println("TOTAL IURAN     = Rp " +total);
        System.out.println("----------------------------------------------------"
                + "-----------------------------------------------------------------------------");
    }

}
