/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

/**
 *
 * @author basisdata
 */
public class Mahasiswa extends Penduduk implements Peserta{

    private String nim;

    public Mahasiswa() {

    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNim() {
        return nim;
    }

    @Override
    public double hitungIuran() {
        double iuran = Integer.parseInt(nim);
        iuran = iuran / 10000;
        return iuran;
    }
    
    @Override
     public String getJenisSertifikat() {
        String sertif = "Panitia";
        return sertif;
    }

    @Override
    public String getFasilitas() {
        String fasilitas = "block note, alat tulis, laptop";
        return fasilitas;
    }

    @Override
    public String getKonsumsi() {
        String konsum = "snack, makan siang, dan makan malam";
        return konsum;
    }

}
