/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul2;

/**
 *
 * @author basisdata
 */
public class UKM {
    
    private String namaUnit;
    private Mahasiswa ketua;
    private Mahasiswa sekretaris;
    private Penduduk[] anggota;
    
    public UKM(){
        
    }

    public void setNamaUnit(String namaUnit) {
        this.namaUnit = namaUnit;
    }

    public void setKetua(Mahasiswa ketua) {
        this.ketua = ketua;
    }

    public void setSekretaris(Mahasiswa sekretaris) {
        this.sekretaris = sekretaris;
    }

    public void setAnggota(Penduduk[] anggota) {
        this.anggota = anggota;
    }

    public String getNamaUnit() {
        return namaUnit;
    }

    public Mahasiswa getKetua() {
        return ketua;
    }

    public Mahasiswa getSekretaris() {
        return sekretaris;
    }

    public Penduduk[] getAnggota() {
        return anggota;
    }
    
    
}
